# [GitLab-Pages-Custom-Domain-Lets-Encrypt.info](https://gitlab-pages-custom-domain-lets-encrypt.info)

## Table of contents

1. [Create a new GitLab Repository and enable GitLab Pages using a GitLab Pages CI script](#1-create-a-new-gitlab-repository-and-enable-gitlab-pages-using-a-gitlab-pages-ci-script)
2. [Add custom domain to GitLab Pages (without certificates) and set the DNS A record](#2-add-custom-domain-to-gitlab-pages-without-certificates-and-set-the-dns-a-record)
3. [Use Let's Encrypt to generate certificate for your domain and re-add custom domain with your Let's Encrypt certificates](#3-use-lets-encrypt-to-generate-certificate-for-your-domain-and-re-add-custom-domain-with-your-lets-encrypt-certificates)
4. [Manually update your certificate](#4-manually-update-your-certificate)
5. [Wrap up and notes](#wrap-up-and-notes)
6. [Things left to do](#things-left-to-do)
7. [Waiver of liability](#waiver-of-liability)
8. [Quick Checklist](#quick-checklist)
9. [How to change the domain's DNS records with various registrars](#how-to-change-the-domains-dns-records-with-various-registrars)
    - [Wedos](#wedos)
    - [Zoner](#zoner)

## 1. Create a new GitLab Repository and enable GitLab Pages using a GitLab Pages CI script

You can use an existing repository. If you already have one, go to [step 2](#2-add-custom-domain-to-gitlab-pages-without-certificates-and-set-the-dns-a-record).
You can use a private GitLab repository.
The published GitLab Pages are always public.
You cannot control the setup of GitLab Pages server so you cannot use HTTP Basic authentication.
You also cannot set up HTTP to HTTPS redirect on the server level and need to rely on JavaScript for that.

If you want to use plain HTML, add the following `.gitlab-ci.yml` script in the root of your repository:
```yml
image: alpine:latest # Super small Docker container. Smaller than default Ruby one which is chosen if this is ommited.
pages:
  stage: deploy
  script:
  # Copy the static assets
  - mkdir .public
  - cp -r * .public # Copies everything in root to artifacts. Alternatively `cp -r src/* .public` copies only the `src` directory.
  # Deploy the final tree as artifacts
  - mv .public public
  artifacts:
    paths:
    - public
  only:
  - master
```

If you want to use a static site generator, check out [this](https://gitlab.com/groups/pages). You can also develop your own `.gitlab-ci.yml`.

Having `.gitlab-ci.yml` in your repository means GitLab CI MultiRunner will pick up your repository after each commit and build it according to `.gitlab-ci.yml`.
On `gitlab.com` you will be using the shared multi-runner.
If you are using your own instance of GitLab running on your infrastructure, things might differ (you can change NGINX setting to force HTTPS and redirect WWW there).
The build artifacts will be deployed to GitLab servers and made accessible at `https://{user/group}.gitlab.io/{repo}`.

## 2. Add custom domain to GitLab Pages (without certificates) and set the DNS A record

If you have enabled Gitlab Pages previously, go to [step 3](#3-use-lets-encrypt-to-generate-certificate-for-your-domain-and-re-add-custom-domain-with-your-lets-encrypt-certificates).
Go to repository settings' *Pages* section and click *New Domain*.
Fill in the name of the domain (such as `gitlab-pages-custom-domain-lets-encrypt.info`) and leave the certificate fields empty.
Go to your domain registrar's administration section and change the domain `A` DNS record to point to `104.208.235.32`.
Do not forget to apply the changes and wait for about 30 minutes to and hour for them to take effect.
You can use [MX Toolbox](https://mxtoolbox.com/) to check the records have been changed. Enter `a:gitlab-pages-custom-domain-lets-encrypt.info` in the input text field.
GitLab doesn't have IPv6 IP address due to it being hosted on AWS and for that reason there's nothing to change the `AAA` record to.
GitLab is moving to its own infrastructure and this might change in the future.
[Link](https://gitlab.com/gitlab-org/gitlab-ce/issues/2481#note_20499243)
[Link](https://gitlab.com/gitlab-com/infrastructure/issues/645)
After the change, you should be able to access your GitLab Pages from both the GitLab Pages domain (such as `gitlab-pages-custom-domain-lets-encrypt.info`) and your own domain.
You will get a certificate error if you access them using HTTPS on your own domain, because the certificate from GitLab is a wildcard for `*.gitlab.io` and not your own domain.

This guide doesn't discuss using `CNAME` to host GitLab Pages on a subdomain.
This is important for `www` subdomain.
Theoretically setting a `CNAME` for `www` is enough but it is not tested as of now.
Due to us not being able to configure the GitLab Pages server, we can't do `www` to non-`www` (or vice-versa) redirect on the server.
This is theoretically doable in JavaScript similarly to how HTTP is redirected to HTTPS in JavaScript.
Feel free to open a PR.

## 3. Use Let's Encrypt to generate certificate for your domain and re-add custom domain with your Let's Encrypt certificates

If you've already generated certificates this method before, go to [step 4](#4-manually-update-your-certificate).

(Use Unix shell or WSL on Windows):

```sh
git clone https://github.com/certbot/certbot
cd certbot
./letsencrypt-auto certonly -a manual -d gitlab-pages-custom-domain-lets-encrypt.info # your domain
```

> [*cannot enable executable stack as shared object requires*](#troubleshooting)

You can add multiple `-d` switches to obtain a certificate for a number of domains or for WWW and non-WWW versions of your domain name (or both).

Follow along and wait to get the ACME challenge. Change `.gitlab-cy.yml`:

```yml
image: alpine:latest # Small Docker image, good for plain HTML.
pages:
  stage: deploy
  script:
  # Copy the static assets
  - mkdir .public
  - cp -r * .public # Or `src/*`
  # Write the Let's Encrypt CA ACME challenge for domain verification
  - mkdir -p .public/.well-known/acme-challenge/
  - echo {challenge (ends with `ok`)} > .public/.well-known/acme-challenge/{path}
  # Deploy the final tree as artifacts
  - mv .public public
  artifacts:
    paths:
    - public
  only:
  - master
```

Wait for the deployment to finish and then confirm in Let's Encrypt.
Your certificates will be created in `/etc/letsencrypt/live/gitlab-pages-custom-domain-lets-encrypt.info/` (your domain directory).

If you are on Windows, use Ubuntu on Windows 10 for this.
To copy certificates to your Windows file system, use this:

```sh
sudo -s # Start interactive sudo session.
# Make sure to use correct domain directory name and to create the target directory in C:/certs beforehand.
cp /etc/letsencrypt/live/gitlab-pages-custom-domain-lets-encrypt.info/* /mnt/c/certs
```

Once you have the four `PEM` files, go to repository settings' *Pages* section again.

- Remove the domain you have entered previously (to change DNS A change worked and you can see your GitLab Pages on your domain when using HTTP).
- Add the domain again as described in the previous step, but this time add `fullchain.pem` to the first certificate field and `privkey.pem` to the second certificate field.

## 4. Manually update your certificate

- Run `./letsencrypt-auto certonly -a manual -d gitlab-pages-custom-domain-lets-encrypt.info` again with one or more `-d` switches with your domain names

> [*cannot enable executable stack as shared object requires*](#troubleshooting)

- Add one or more `echo` statements to your `.gitlab-ci.yml` with LE ACME challenges for all the domain names you have entered
- Copy your certificates over from Ubuntu on Windows 10 environments to your directory on Windows: `sudo -s` followed by `cp /etc/letsencrypt/live/gitlab-pages-custom-domain-lets-encrypt.info/* /mnt/c/certs`
- Go to *Settings*  *Pages* section of your GitLab project and replace domains with new associations with updated `fullchain.pem` and `privkey.pem`
- Use incognito mode in Google Chrome to verify the new certificate works correctly in the *Security* section of the Developer Tools
  - Alternatively [Qualys SSL Labs Server Test][ssl-test] can be used to inspect and verify the properties of the certificate

- [ ] Script draft

```sh
domainName=gitlab-pages-custom-domain-lets-encrypt.net
userName=TomasHubelbauer
projectName=gitlab-pages-custom-domain-lets-encrypt-info

# Generate the updated certificate
./letsencrypt-auto certonly -a manual -d $domainName

# TODO: Get the new data from the stdout somehow - regex or letsencrypt-auto switch or a different script?
challengeFull=todo
challengeShort=todo

# Pull the repository down
if cd $projectName; then git pull origin master; else git clone https://gitlab.com/$userName/$projectName.git; fi

# Replace the `echo` line with LE ACME challenge data
# TODO: `sed` 'echo\s"[\w.-]+"\s>\s.well-known/acme-challenge/\w+' 'echo "$challengeFull" > .well-known/acme-challenge/$challengeShort'

# Upload new LE ACME challenge file
git add .gitlab-ci.yml
git commit -m "Update Let's Encrypt ACME challenge"
git push origin master

# Wait until GitLab Pages pipeline has completed
# TODO: https://superuser.com/a/375331/490452
watch until curl the GitLab Pages URL + .well-known/acme-challenge/$challengeShort

# TODO: SSL test
```

[ssl-test]: https://www.ssllabs.com/ssltest/index.html

### Why not use [`gitlab-letsencrypt`](https://github.com/rolodato/gitlab-letsencrypt)?

It looks promising, but it doesn't work on Windows out of the box.

### Why not use GitLab Pages API

It didn't support managing certificates through the API when this project started.
Now it seems to!

- [ ] Explore [GitLab Pages API](https://docs.gitlab.com/ee/api/pages_domains.html) for this

## Wrap up and notes

- The Let's Encrypt certificate is valid for 3 month. You will have to periodically redo this unless you automate it. The basic idea behind it is below but it is untested.

```sh
# 1. Invoke Let's Encrypt with the domain name and automatically confirm prompts
# 2. Read the ACME challenge, edit `.gitlab-ci.yml` or change a standalone file if `.gitlab-ci.yml` just copies it over as with other files (`cp` doesn't copy dotfiles by default)
# 3. Push the change to GitLab using Git and wait for the challenge file to be up using something like `watch` or another `cron`
# 4. Continue the Let's Encrypt command now that the file is up
# 5. Copy the cert files somewhere and mail yourself a reminder to change the custom domain settings on GitLab
```

- HTTP to HTTPS redirect can be done using JavaScript:

```js
if (location.protocol !== 'https:') {
    location = location.href.replace(/^http:/, "https:");
}
```

- WWW to non-WWW redirect can be done using JS also as long as you set up the CNAME record (untested, you will probably need to get a certificate for WWW as well). On second thought you can only enter one certificate with GitLab Pages and Let's Encrypt won't give you a wildcard so this is probably impossible. There might be a way to do this with different GitLab Pages for non-WWW and WWW where the WWW GitLab Pages redirect to the non-WWW URL.
- It is good to use the `base` meta tag with the canonical URL set to the HTTPS version so Google doesn't index your HTTP version.
- It is good to use the `canonical` meta tag for each page with the HTTPS URL. This is only practical if you are using static site generator and not plain HTML because you would have to do it yourself.
- Do not load HTTP resources as part of your page on HTTPS otherwise the page will rightfully not be marked as secure.
- Do not use the `//` protocol relative [antipattern](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/4342/diffs). Use HTTPS explicitly everywhere you know it works. HTTPS page with HTTP resources sucks, HTTP page with HTTPS resources less so, HTTPS everywhere is the way to go.

## Troubleshooting

### *cannot enable executable stack as shared object requires*

https://github.com/Microsoft/WSL/issues/916

```bash
sudo apt-get install execstack
sudo execstack -c /opt/eff.org/certbot/venv/local/lib/python2.7/site-packages/cryptography/hazmat/bindings/_openssl.so # Whatever `so` file is problematic, most likely OpenSSL
```

### *ConnectionError: ('Connection aborted.', BadStatusLine("''",))*

- [ ] Resolve this!

Uninstall and install WSL again: `lxrun.exe /uninstall /full` followed by `lxrun.exe /install`.

If this fails, use PowerShell to remove `%LocalAppData%/lxss` manually:

```powershell
cd $env:LOCALAPPDATA
rm -r lxss
```

If that fails just fuck it and use https://aka.ms/wslstore.
This seems to install a different, working installation of WSL.

…aaaand it still doesn't work. Says the URL returns a 404 where `curl` on the same host fetches it perfectly fine.

## Things left to do

- [ ] Update with info based on experimenting with LE SAN (specifying `-d` multiple times) and issuing certs for both @ and www.
  - `A` for root.
  - `CNAME` for `www`.
  - `./letsencrypt-auto certonly -a manual -d gitlab-pages-custom-domain-lets-encrypt.info -d www.gitlab-pages-custom-domain-lets-encrypt.info # your domain`
  - Redirect in JavaScript.
  - [This section describes both.](https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains)
  - https://certbot.eff.org/docs/using.html#changing-a-certificate-s-domains for info on non-www and www
- [ ] For some reason specyfing `A` for both `www.domain.net` and `domain.net` causes `www` to non-`www` redirect? This is even without LE SAN.

## Waiver of liability

This is a result of my experimentation as someone who doesn't know anything about anything. I am sure there's a million things wrong with this.
Let me know and I'll fix it and attribute the fix to you. [@TomasHubelbauer](https://twitter.com/tomashubelbauer).
[MRs welcome](https://gitlab.com/TomasHubelbauer/gitlab-pages-custom-domain-lets-encrypt-info)

## Quick Checklist

If you've come here again to make sure you've followed all the steps for your repository, refer to this quick checklist:

1. `.gitlab-ci.yml`, `http://{user/group}.gitlab.io/{repo}`, `A = 104.208.235.32`, `fullchain.pem`, `privkey.pem`, `https://{domain}.{tld}`
2. `if (location.protocol !== 'https:') location = location.href.replace(/^http:/, "https:");`
3. `<base href='https://{domain}.{tld}' />`, `<link rel='canonical' href='https://{domain}.{tld}/{path}/' />`

## How to change the domain's DNS records with various registrars

This is a rundown of how to change a domain name's DNS records with various registrars. Send a PR with your own.

### Wedos

Go to your Wedos administration section.
Click on *Domains* and find your domain.
Click on *Change DNS records* to the right.
Find the `A` record and change the IP address.
Click on *Apply changes* above the record table.
You are done, wait for and hour for the change to propagate.
You might want to cancel your webhosting service so you don't have to pay for it anymore, if you had one. For GitLab Pages on custom domain you only need the domain service.

### Zoner

Log in to Zoner admin section.
Find you domain name in the list of services and click on *Detail*.
Switch to *Available Operations* tab and choose *DNS Settings*.
Put GitLab's IP address from above into both `www` and `@` fields in the `A` record section.

- [ ] I guess only `@` since we won't get a cert for `www` anyway - can't enter that in GitLab Pages and Let's Encrypt won't issue a wildcard.

---------------------------------------

Created by [Tomas Hubelbauer](https://TomasHubelbauer.net)
⋮
[Edit this page](https://gitlab.com/TomasHubelbauer/gitlab-pages-custom-domain-lets-encrypt-info/edit/master/README.md)